setupATLAS

asetup AthDerivation,21.2.89.0,slc6,here
#asetup AthDerivation,21.2.38.0,here #original 'freshstart'
#asetup AthDerivation,21.2.85.0,here    ##### >>>>>>> huffmanFixes

mkdir -p run

cd run

for JO in ../btagAnalysis/share/*.py; do
    ln -sf $JO
done

cd ..

if [[ ! -d build ]] ; then
    ./scripts/build.sh
else
    echo 'already built, run `./scripts/build.sh` to rebuild'
fi

source build/x86_64-centos7-gcc62-opt/setup.sh 
