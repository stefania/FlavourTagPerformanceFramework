#ifndef BTAG_TRACK_ACCESSORS_HH
#define BTAG_TRACK_ACCESSORS_HH

#include "AthContainers/AuxElement.h"
// #include "GeoPrimitives/GeoPrimitives.h"

struct BTagTrackAccessors {
  //SG::AuxElement::ConstAccessor< float > z0_raw;
  SG::AuxElement::ConstAccessor< float > d0;
  SG::AuxElement::ConstAccessor< float > z0;
  SG::AuxElement::ConstAccessor< float > d0_sigma;
  SG::AuxElement::ConstAccessor< float > z0_sigma;

  SG::AuxElement::ConstAccessor< std::vector<float> > displacement;
  SG::AuxElement::ConstAccessor< std::vector<float> > momentum;

  BTagTrackAccessors():
    //z0_raw("btagIp_z0SinTheta"),//<BTH> was btag_z0 which works in 21.2.38
    d0("btagIp_d0"), //<BTH> was btag_ip_d0 which works in 21.2.38
    z0("btagIp_z0SinTheta"), //<BTH> was btag_ip_z0 which works in 21.2.38
    d0_sigma("btagIp_d0Uncertainty"),//<BTH> was btag_ip_d0_sigma which works in 21.2.38
    z0_sigma("btagIp_z0SinThetaUncertainty"),//<BTH> was btag_ip_z0_sigma which works in 21.2.38
    displacement("btagIp_trackDisplacement"),//<BTH> was btag_track_displacement which works in 21.2.38
    momentum("btagIp_trackMomentum")//<BTH> was btag_track_momentum which works in 21.2.38
   {
    }

};

#endif
